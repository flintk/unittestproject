/**
 * @author Keller Flint
 *
 * Scorer for a game of yahtzee.
 */
public class YahtzeeScorer {

    int score;
    int yahtzeeCounter;

    boolean threeOfAKind;
    boolean fullHouse;
    boolean largeStraight;

    /**
     * Initializes Yahtzee scorer with default values.
     */
    public YahtzeeScorer() {
       this.score = 0;
       this.yahtzeeCounter = 0;

       this.threeOfAKind = false;
       this.fullHouse = false;
       this.largeStraight = false;
    }

    /**
     * Returns highest integer of the given values.
     * @param dice
     * @return int highest number
     */
    public int findHigh(int[] dice) {
        int high = dice[0];
        for (int x = 1; x < dice.length; x++) {
            if (high < dice[x]) {
                high = dice[x];
            }
        }
        return high;
    }

    /**
     * Checks if the value of the dice passed constitute a valid Large Straight.
     * @param dice
     * @return boolean true if successful
     */
    public boolean isLargeStraight(int[] dice) {

        // Ensures the category has not already been scored
        if (largeStraight)
            return false;

        // Streak increases by one every time the program finds another sequential value
        int streak = 0;
        for (int x = 0; x < dice.length; x++) {
            for (int y = 1; y <= dice.length; y++) {
                if (dice[y - 1] + 1 == dice[x])
                    streak++;

            }
        }

        if (streak == 4) {
            setScore(40);
            largeStraight = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the value of the dice passed constitute a valid Yahtzee.
     * @param dice
     * @return boolean true if successful
     */
    public boolean isYahtzee(int[] dice) {

        // Streak increases by one every time the program finds another equivalent value
        int streak = 0;
        for (int x = 1; x < dice.length; x++) {
            if (dice[0] == dice[x])
                streak++;
        }

        if (streak == 4) {
            if (yahtzeeCounter < 1)
                setScore(50);
            else
                setScore(100);
            yahtzeeCounter++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the value of the dice passed constitute a valid Three of a Kind.
     * @param dice
     * @return boolean true if successful
     */
    public boolean isThreeOfAKind(int[] dice) {

        // Ensures the category has not already been scored
        if (threeOfAKind)
            return false;

        // Counts how many of each number occur in the dice array
        int[] bucket = new int[7];
        for (int x = 0; x < dice.length; x++) {
            bucket[dice[x]]++;
        }

        if (findHigh(bucket) >= 3) {
            setScore(20);
            threeOfAKind = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the value of the dice passed constitute a valid Full House.
     * @param dice
     * @return boolean true if successful
     */
    public boolean isFullHouse(int[] dice) {

        // Ensures the category has not already been scored
        if (fullHouse)
            return false;

        // Counts how many of each number occur in the dice array
        int[] bucket = new int[7];
        for (int x = 0; x < dice.length; x++) {
            bucket[dice[x]]++;
        }

        int total = 0;
        for (int x = 0; x < bucket.length; x++) {
            if (bucket[x] == 2 || bucket[x] == 3)
                total += bucket[x];
        }

        if (total == 5) {
            setScore(25);
            fullHouse = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the current score of the game.
     * @return score
     */
    public int getScore() {
        return this.score;
    }

    /**
     * Adds to the current score by the given value.
     * @param score
     * @return boolean true if successful
     */
    public boolean setScore(int score) {
        // Prevents negative scores
        if (score > 0) {
            this.score += score;
            return true;
        } else {
            return false;
        }
    }

}
