import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * @author Keller Flint
 */
public class YahtzeeTest {

    @Test
    public void testInstantiation() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
    }

    @Test
    public void testFindHigh() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {1, 3, 9, 7, 5, 2};
        assertEquals(9, yahtzeeScorer.findHigh(dice));
    }

    @Test
    public void testIsLargeStraightTrue() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {2, 5, 3, 1, 4};
        assertEquals(true, yahtzeeScorer.isLargeStraight(dice));
    }

    @Test
    public void testIsLargeStraightFalse() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {2, 6, 3, 1, 4};
        assertEquals(false, yahtzeeScorer.isLargeStraight(dice));
    }

    @Test
    public void testIsYahtzeeTrue() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {3, 3, 3, 3, 3};
        assertEquals(true, yahtzeeScorer.isYahtzee(dice));
    }

    @Test
    public void testSecondYahtzee() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {3, 3, 3 , 3, 3};
        assertEquals(true, yahtzeeScorer.isYahtzee(dice));
        assertEquals(true, yahtzeeScorer.isYahtzee(dice));

    }

    @Test
    public void testIsYahtzeeFalse() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {1,3,3,3,3};
        assertEquals(false, yahtzeeScorer.isYahtzee(dice));
    }
    @Test
    public void testThreeOfAKindTrue() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {3, 1, 3, 2, 3};
        assertEquals(true, yahtzeeScorer.isThreeOfAKind(dice));
    }

    @Test
    public void testThreeOfAKindFalse() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {2, 1, 3, 2, 3};
        assertEquals(false, yahtzeeScorer.isThreeOfAKind(dice));
    }

    @Test
    public void testFullHouseTrue() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {5, 3, 3, 5, 3};
        assertEquals(true, yahtzeeScorer.isFullHouse(dice));
    }

    @Test
    public void testFullHouseFalse() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {1, 3, 3, 5, 3};
        assertEquals(false, yahtzeeScorer.isFullHouse(dice));
    }

    @Test
    public void testGetScore() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        yahtzeeScorer.setScore(20);
        assertEquals(20, yahtzeeScorer.getScore());
    }

    @Test
    public void testIfFullHouseScored() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {3, 4, 4, 3, 3};
        assertEquals(true, yahtzeeScorer.isFullHouse(dice));
        assertEquals(false, yahtzeeScorer.isFullHouse(dice));
    }

    @Test
    public void testIfLargeStraightScored() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {1, 2, 3, 4, 5};
        assertEquals(true, yahtzeeScorer.isLargeStraight(dice));
        assertEquals(false, yahtzeeScorer.isLargeStraight(dice));
    }

    @Test
    public void testIfThreeOfAKindScored() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        int dice[] = {1, 1, 1, 4, 5};
        assertEquals(true, yahtzeeScorer.isThreeOfAKind(dice));
        assertEquals(false, yahtzeeScorer.isThreeOfAKind(dice));
    }

    @Test
    public void testSetScoreTrue() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        assertEquals(true, yahtzeeScorer.setScore(10));
    }

    @Test
    public void testSetScoreFalse() {
        YahtzeeScorer yahtzeeScorer = new YahtzeeScorer();
        assertEquals(false, yahtzeeScorer.setScore(-10));
    }

}